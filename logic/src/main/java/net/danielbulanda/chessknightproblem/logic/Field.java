package net.danielbulanda.chessknightproblem.logic;

public class Field {
    private int x, y;

    public Field() {}

    public Field(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Field(Field field) {
        if (field != null) {
            x = field.x;
            y = field.y;
        }
    }

    public int getX() { return x; }

    public int getY() { return y; }

    Field move(int[] by) {
        return new Field(x + by[0], y + by[1]);
    }

    boolean isOnTheBoard(Board board) {
        return 0 <= x & x < board.getWidth() & 0 <= y & y < board.getHeight();
    }

    boolean visited(Board board) {
        return board.getVisitsOrder(x, y) != 0;
    }

    int numberOfExits(Knight knight, Board board) {
        int result = 0;
        for (int i = 0; i < knight.getSteps().length; i++)
        {
            Field next = move(knight.getSteps()[i]);
            if (next.isOnTheBoard(board) && !next.visited(board))
                result++;
        }
        return result;
    }

    boolean isOnTheSamePlace(Field field) {
        if (field.getX() == getX() && field.getY() == getY()) {
            return true;
        }
        return false;
    }
}
