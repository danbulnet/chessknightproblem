package net.danielbulanda.chessknightproblem.logic;

public class MainLogic {
    private Board board;
    private Knight knight;
    private final int BOARD_WIDTH;
    private final int BOARD_HEIGHT;

    public MainLogic(int boardWidth, int boardHeight) {
        BOARD_WIDTH = boardWidth;
        BOARD_HEIGHT = boardHeight;
    }

    public Board getBoard() { return board; }
    public Knight getKnight() { return knight; }

    private void resetSearch() {
        this.board = new Board(BOARD_WIDTH, BOARD_HEIGHT);
        int x = (int)(board.getWidth() * Math.random());
        int y = (int)(board.getHeight() * Math.random());
        Field location = new Field(x, y);
        this.knight = new Knight(location, board);
    }

    public Board findPath(boolean closed) {
        int iterations = 0;
        boolean shouldSearch = true;
        while (shouldSearch) {
            resetSearch();
            while (knight.canMove(board)) {
                knight.move(board);
            }
            shouldSearch = (knight.isPathClosed(board) != closed || !board.isTourComplete()) && iterations < 100000;
            iterations++;
        }
        return board;
    }
}
