package net.danielbulanda.chessknightproblem.logic;

public class Board {
    private final int BOARD_WIDTH;
    private final int BOARD_HEIGHT;
    private int[][] visitsOrder;
    private int numberOfVisitedPoints;
    private Field firstField;

    public Board(final int BOARD_WIDTH, final int  BOARD_HEIGHT) {
        this.BOARD_WIDTH = BOARD_WIDTH;
        this.BOARD_HEIGHT = BOARD_HEIGHT;
        visitsOrder = new int [BOARD_WIDTH] [BOARD_HEIGHT];

        for (int i = 0; i < BOARD_WIDTH; i++)
            for (int j=0; j < BOARD_HEIGHT; j++) {
                visitsOrder[i][j] = 0;
            }
        numberOfVisitedPoints = 0;
    }

    public int getWidth() { return BOARD_WIDTH; }
    public int getHeight() { return BOARD_HEIGHT; }
    public int getVisitsOrder(int x, int y) { return visitsOrder[x][y]; }
    public Field getFirstField() { return firstField; }

    void placeOnField(Field field) {
        numberOfVisitedPoints++;
        visitsOrder[field.getX()] [field.getY()] = numberOfVisitedPoints;
        if (numberOfVisitedPoints == 1) {
            firstField = new Field(field.getX(), field.getY());
        }
    }

    public boolean isTourComplete() {
        boolean isComplete = true;
        for (int i = 0; i < BOARD_WIDTH; i++) {
            for (int j = 0; j < BOARD_HEIGHT; j++) {
                if (visitsOrder[i][j] == 0) {
                    isComplete = false;
                }
            }
        }
        return isComplete;
    }

    public String toString() {
        String result = "";
        for (int row = BOARD_WIDTH - 1; row >= 0; row--) {
            for (int col = 0; col < BOARD_HEIGHT; col++) {
                if (visitsOrder[row][col] < 10)
                    result += " ";
                result += visitsOrder[row][col] + " ";
            }
            result += "\n";
        }
        return result;
    }
}
