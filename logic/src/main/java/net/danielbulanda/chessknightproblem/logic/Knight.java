package net.danielbulanda.chessknightproblem.logic;

public class Knight {
    private final int[][] STEPS = {{2, 1}, {1, 2}, {-1, 2}, {-2, 1}, {-2, -1}, {-1, -2}, {1, -2}, {2, -1}};
    private Field field;

    public Knight(Field field, Board board) {
        placeOnField(field, board);
    }

    int[][] getSteps() {
        return STEPS;
    }

    private void placeOnField(Field location, Board board) {
        field = new Field(location);
        board.placeOnField(field);
    }

    public boolean canMove(Board board) {
        return field.numberOfExits(this, board) > 0;
    }

    public void move(Board board) {
        int minExits = minNumberOfExits(board);
        int possibilities = numberOfPossibilities(minExits, board);
        int which = (int) (possibilities * Math.random() + 1);
        Field next = newLocation(which, board, minExits);
        placeOnField(next, board);
    }

    private int minNumberOfExits(Board board) {
        int exits = STEPS.length + 1;
        for (int i = 0; i < STEPS.length; i++) {
            Field next = field.move(getSteps()[i]);
            if (next.isOnTheBoard(board) && !next.visited(board)) {
                exits = Math.min(exits, next.numberOfExits(this, board));
            }
        }
        return exits;
    }

    private int numberOfPossibilities(int exits, Board board) {
        int number = 0;
        for (int i = 0; i < STEPS.length; i++) {
            Field next = field.move(getSteps()[i]);
            if (next.isOnTheBoard(board) && !next.visited(board) && next.numberOfExits(this, board) == exits) {
                number++;
            }
        }
        return number;
    }

    private Field newLocation(int which, Board board, int exits) {
        int number = 0, i = 0;
        Field next = new Field();
        while (number < which) {
            next = field.move(getSteps()[i]);
            if (next.isOnTheBoard(board) && !next.visited(board) && next.numberOfExits(this, board) == exits) {
                number++;
            }
            i++;
        }
        return next;
    }

    public boolean isPathClosed(Board board) {
        if (board.getFirstField() != null) {
            for (int i = 0; i < STEPS.length; i++) {
                Field next = field.move(getSteps()[i]);
                if (next.isOnTheBoard(board) && next.isOnTheSamePlace(board.getFirstField())) {
                    return true;
                }
            }
        }
        return false;
    }
}
